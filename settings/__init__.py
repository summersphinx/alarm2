import PySimpleGUI as sg
import urllib3
import json
import os

http = urllib3.PoolManager()

time_format_guide = '''Formatting Guidelines:

Must lead with either 12 or 24. This signals either to use 12hr or 24hr format.

H             > Show the hour.
HH           > Show the hour. Adds a leading 0 if it is single digit.
M            > Shows minutes.
MM         > Shows minutes. Adds a leading 0 if it is a single digit.
S             > Shoes seconds.
SS           > Shows seconds. Adds a leading 0 if it is a single digit.
:              > Adds a colon as a divider.
-             > Adds a dash as a divider.
<s>        > Adds a space as a divider.
<n>        > New Line.
<song>  > Shows the name of currently playing song.
<art>     > Shows the name of the artist for the song currently playing.

Default: 12HH:MM:SS<n><song><s>-<s><art>
'''

license_urls = {
    'STOOLS': 'https://gitlab.com/summersphinx/alarm2/-/raw/master/LICENSE',
    'SELENIUM': 'https://github.com/SeleniumHQ/seleniumhq.github.io/raw/trunk/LICENSE'
}
license_text = {}
for each in license_urls:
    license_text[each] = http.request('GET', license_urls[each]).data.decode("utf-8")


def settings_window(title='Settings', first_run=True, external_keyboard=True, fullscreen=True, colorway='Default', puzzles=True, default_brightness=100, night_brightness=50, time_before_brightness_change=5, flashbomb=True, follow_night_cycle=True, time_to_dim=2200, time_to_brighten=600, time_format="12HH:MM:SS<n><song><s>-<s><art>", math=True, math_solves=3, wordle=True, wordle_solves=3, coderandom=True, coderandom_solves=3, coderandom_difficulty='Hard'):
    main_tab = [
        [sg.Multiline(license_text['STOOLS'], disabled=True, s=(80, 40))]
    ]

    license_tab = [
        [sg.Text(
                'Every license for this program. By clicking next, you agree to each of the different licenses provided.')],
        [sg.TabGroup([
            [
                sg.Tab('STOOLS', [
                    [sg.Multiline(license_text['STOOLS'], disabled=True, s=(80, 40))]
                ]),
                sg.Tab('SELENIUM', [
                    [sg.Multiline(license_text['SELENIUM'], disabled=True, s=(80, 40))]
                ]),
            ]
        ])]
    ]

    brightness_tab = [
        [sg.Text('Change the brightness settings of the program.')],
        [sg.Text('Brightness:         ', tooltip='The default brightness of the screen.'),
         sg.Slider((0, 100), default_brightness, 2, orientation='h', k='default_brightness')],
        [sg.Text('Night Brightness:', tooltip='The default brightness of the screen during night.'),
         sg.Slider((0, 100), night_brightness, 2, orientation='h', k='night_brightness')],
        [sg.Text('Change Time:     ',
                 tooltip='The amount of time before the screen will change brightness levels before the alarm.'),
         sg.Spin([i for i in range(60)], time_before_brightness_change, s=(2, 1), k='time_before_brightness_change'), sg.Text("Minutes")],
        [sg.Text()],
        [sg.Checkbox('Flashbomb Screen', default=flashbomb, tooltip='Change the color palette to a light theme when the alarm is set off.',
                     k='flashbomb')],
        [sg.Text()],
        [sg.Checkbox('Follow Night Cycle', default=follow_night_cycle, tooltip='Brighten and dim the screen along with the daylight cycle.',
                     k='follow_night_cycle')],
        [sg.Text('Dim Time:        ', tooltip='If night cycle is off, the time the screen will dim.'),
         sg.Input(time_to_dim, k='time_to_dim')],
        [sg.Text('Brighten Time:  ', tooltip='If night cycle is off, the time the screen will brighten.'),
         sg.Input(time_to_brighten, k='time_to_brighten')],
    ]

    general_tab = [
        [sg.Text('Modify the default characteristics of the alarm.')],
        [sg.Checkbox('External Keyboard', default=external_keyboard, tooltip='Check the box if there is an external keyboard connected.',
                     k='external_keyboard')],
        [sg.Checkbox('Full Screen', default=fullscreen, tooltip='Make the window fullscreen borderless.', k='fullscreen')],
        [sg.Text()],
        [sg.Text('Color'), sg.DropDown(['Default'], colorway, tooltip='Change the color of the alarm.', k='colorway')],
        [sg.Text()],
        [sg.Checkbox('Enable Puzzles', tooltip='Enable puzzles to turn off alarms.', default=puzzles, k='puzzles')]
    ]

    advanced_tab = [
        [sg.Text('Advanced settings for the alarm clock.')],
        [sg.Text('Time Format', tooltip=time_format_guide),
         sg.Input(time_format, expand_x=True, k='time_format', tooltip=time_format_guide)],
        [sg.Text()],
        [sg.Checkbox('Math            ', default=math, k='math'), sg.Text('Solves Needed:'),
         sg.Input(math_solves, k='math_solves')],
        [sg.Checkbox('Wordle         ', default=wordle, k='wordle'), sg.Text('Solves Needed:'),
         sg.Input(wordle_solves, k='wordle_solves')],
        [sg.Checkbox('CodeRandom', default=coderandom, k='coderandom'), sg.Text('Solves Needed:'),
         sg.Input(coderandom_solves, k='coderandom_solves'), sg.DropDown(['Easy', 'Medium', 'Hard'], coderandom_difficulty, k='coderandom_difficulty')]
    ]

    confirm_tab = [
        [sg.Multiline(k='final', disabled=True, expand_x=True, expand_y=True)]
    ]

    tabgroup = [[sg.Tab('Main       ', main_tab, k='main'), sg.Tab('Licenses ', license_tab, k='licenses'),
                 sg.Tab('General   ', general_tab, k='general'), sg.Tab('Display   ', brightness_tab, k='display'),
                 sg.Tab('Advanced', advanced_tab, k='advanced'), sg.Tab('Confirm', confirm_tab, k='confirm')]]
    tabs = ['main', 'licenses', 'general', 'display', 'advanced', 'confirm']

    layout = [
        [sg.Text('Alarm Setup')],
        [sg.TabGroup(tabgroup, tab_location='lefttop', k='main_tabgroup')],
        [sg.Text('', expand_x=True), sg.Button('< Back <', k='back'), sg.Button('> Next >', k='next'),
         sg.Button('= Done =', k='done', disabled=first_run)]
    ]

    wn = sg.Window(title=title, layout=layout, finalize=True)
    settings_data = None

    while True:
        if tabs.index(wn['main_tabgroup'].get()) == 0:
            wn['back'].update(disabled=True)
        else:
            wn['back'].update(disabled=False)
        if tabs.index(wn['main_tabgroup'].get()) == len(tabgroup[0]) - 1:
            wn['next'].update(disabled=True)
        else:
            wn['next'].update(disabled=False)

        datatree = {
            'General' : {
                'External Keyboard': wn['external_keyboard'].get(),
                'Fullscreen'       : wn['fullscreen'].get(),
                'Color'            : wn['colorway'].get(),
                'Puzzles'          : wn['puzzles'].get()
            },
            'Display' : {
                'Brightness'         : wn['default_brightness'].Widget.get(),
                'Brightness(night)'  : wn['night_brightness'].Widget.get(),
                'Change before alarm': wn['time_before_brightness_change'].get(),
                'Flashbomb'          : wn['flashbomb'].get(),
                'Follow Night Cycle' : wn['follow_night_cycle'].get(),
                'Dim Time'           : wn['time_to_dim'].get(),
                'Brighten Time'      : wn['time_to_brighten'].get(),
            },
            'Advanced': {
                'Time Format'          : wn['time_format'].get(),
                'Math'                 : wn['math'].get(),
                'Math Solves'          : wn['math_solves'].get(),
                'Wordle'               : wn['wordle'].get(),
                'Wordle Solves'        : wn['wordle_solves'].get(),
                'CodeRandom'           : wn['coderandom'].get(),
                'CodeRandom Solves'    : wn['coderandom_solves'].get(),
                'CodeRandom Difficulty': wn['coderandom_difficulty'].get(),
            }
        }
        wn['final'].update(json.dumps(datatree, indent=3))

        event, values = wn.read()
        if event == sg.WIN_CLOSED:
            break

        if values['main_tabgroup'] == 'licenses':
            wn['done'].update(disabled=False)

        if event == 'next':
            key = tabs.index(wn['main_tabgroup'].get()) + 1
            wn[tabs[key]].update(disabled=False)
            wn['main_tabgroup'].Widget.select(key)

        if event == 'back':
            key = tabs.index(wn['main_tabgroup'].get()) - 1
            wn['main_tabgroup'].Widget.select(key)

        if event == 'done':
            settings_data = {
                'external_keyboard': wn['external_keyboard'].get(),
                'fullscreen': wn['fullscreen'].get(),
                'colorway': wn['colorway'].get(),
                'puzzles': wn['puzzles'].get(),
                'default_brightness': wn['default_brightness'].Widget.get(),
                'night_brightness': wn['night_brightness'].Widget.get(),
                'time_before_brightness_change': wn['time_before_brightness_change'].get(),
                'flashbomb': wn['flashbomb'].get(),
                'follow_night_cycle': wn['follow_night_cycle'].get(),
                'time_to_dim': wn['time_to_dim'].get(),
                'time_to_brighten': wn['time_to_brighten'].get(),
                'time_format': wn['time_format'].get(),
                'math': wn['math'].get(),
                'math_solves': wn['math_solves'].get(),
                'wordle': wn['wordle'].get(),
                'wordle_solves': wn['wordle_solves'].get(),
                'coderandom': wn['coderandom'].get(),
                'coderandom_solves': wn['coderandom_solves'].get(),
                'coderandom_difficulty': wn['coderandom_difficulty'].get(),
            }
            break
    wn.close()
    return settings_data


class Settings:

    def __init__(self, system):
        if system == 'linux':
            path = 'home/.xplusgames/alarm2'
        elif system == 'win':
            path = os.getenv('LOCALAPPDATA') + '/Xplus Games/Alarm2'

        os.makedirs(path, exist_ok=True)
        os.chdir(path)

        if not os.path.isfile('config.json'):
            settings_tree = settings_window(title='First Time Setup')
            if settings_tree is not None:
                with open('config.json', 'w') as fh:
                    fh.write(json.dumps(settings_tree, indent=3))
                    self.status = 200
                    self.response = 'Ok'
            else:
                print('User Quit out of Setup! Returning None!')
                self.status = 410
                self.response = 'Gone (User quit First Time Setup'
        else:
            with open('config.json', 'r') as fh:
                settings_tree = json.loads(fh.read())
                self.status = 200
                self.response = 'Ok'

        if self.status == 200:
            self.external_keyboard = settings_tree["external_keyboard"],
            self.fullscreen = settings_tree["fullscreen"],
            self.colorway = settings_tree["colorway"],
            self.puzzles = settings_tree["puzzles"],
            self.default_brightness = settings_tree["default_brightness"],
            self.night_brightness = settings_tree["night_brightness"],
            self.time_before_brightness_change = settings_tree["time_before_brightness_change"],
            self.flashbomb = settings_tree["flashbomb"],
            self.follow_night_cycle = settings_tree["follow_night_cycle"],
            self.time_to_dim = settings_tree["time_to_dim"],
            self.time_to_brighten = settings_tree["time_to_brighten"],
            self.time_format = settings_tree["time_format"],
            self.math = settings_tree["math"],
            self.math_solves = settings_tree["math_solves"],
            self.wordle = settings_tree["wordle"],
            self.wordle_solves = settings_tree["wordle_solves"],
            self.coderandom = settings_tree["coderandom"],
            self.coderandom_solves = settings_tree["coderandom_solves"],
            self.coderandom_difficulty = settings_tree["coderandom_difficulty"]

    def ChangeSettings(self):
        settings_tree = settings_window('Setup', False, self.external_keyboard,self.fullscreen,self.colorway,self.puzzles,self.night_brightness,self.flashbomb,self.time_to_dim,self.time_format,self.math,self.math_solves,self.wordle,self.wordle_solves,self.coderandom,self.coderandom_solves,self.coderandom_difficulty)
        if settings_tree is not None:
            with open('config.json', 'w') as fh:
                fh.write(json.dumps(settings_tree, indent=3))
                self.status = 200
                self.response = 'Ok'
        print(settings_tree)


Settings('win').ChangeSettings()
