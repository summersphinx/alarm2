import PySimpleGUI
import time
import calendar
import pygame
from pytube import YouTube

DEBUG = True  # Enable DEBUG Mode.

DEFAULT_BRIGHTNESS = 100  # The brightness of the screen in the day
NIGHT_BRIGHTNESS = 50  # The screen brightness during the night
TIME_BEFORE_BRIGHTNESS_CHANGE = 5  # Minutes to brighten the screen before the alarm goes off
FLASHBOMB_SCREEN = True  # Turn the screen white when the alarm goes off
FOLLOW_NIGHT_CYCLE = True  # Auto get and change the screen brightness based off sunrise and sunset
TIME_TO_DIM = 2200  # Time to manually dim the screen if FOLLOW_NIGHT_CYCLE is off. Must be in 24hr format
TIME_TO_BRIGHTEN = 600  # Time to manually brighten the screen if FOLLOW_NIGHT_CYCLE is off. Must be in 24hr format

EXTERNAL_KEYBOARD = True

TIME_FORMAT = '12HH:MM:SS'  # The format in which to show the time. 12 or 24 must be first to format the hour. Separators are signaled by the colon[:]. If 2 letters are given, a 0 will be filled in(ie. HH = 06). Add a new line with [\n]
SHOW_SONG_NAME = True  # Show the current song being played.
COLORWAY = 'DEFAULT'  # The colorway for the clock

PATH_TO_MUSIC = ''  # Custom path for music

ENABLE_PUZZLES = True

WORDLE_ENABLED = True
WORDLE_SOLVES_NEEDED = 3

MATH_ENABLED = True
MATH_SOLVES_NEEDED = 3

CODERANDOM_ENABLED = True
CODERANDOM_SOLVES_NEEDED = 1
CEASAR_ENABLED = True
TAP_ENABLED = True
PIG_ENABLED = True

''' Youtube Download Test Code
yt = YouTube('https://www.youtube.com/watch?v=YTQV48V44Sw')

print(yt.streams.filter(only_audio=True)[-1])

stream = yt.streams.get_audio_only()
stream.download()
'''

